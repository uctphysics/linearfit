/// \file fit.cpp
/// \author Michael Malahe
/// \brief Source for fitting functions.

#include "fit.h"
#include "fitcurve.h"

//Utility
double bisection(double (*func)(double),double a,double b,double tol){
	double c = (a+b)/2;
	double fa = func(a);
	double fb = func(b);
	double fc = func(c);	
	if (fabs(fc) < tol){//Check for convergence
		return c;
	}
	else {
		if (fa*fc<0){
			b = c;
		}
		else if (fb*fc<0){
			a = c;
		}
		else{
			;//todo: put some kind of error raising here?
		}
	}
	return bisection(func,a,b,tol);//Recurse
}

double bisection(double (*func)(QVector<double>,QVector<double>,QVector<double>,QVector<double>,double),QVector<double> v1,QVector<double> v2,QVector<double> v3,QVector<double> v4,double a,double b,double tol){
	double c = (a+b)/2;
	double fa = func(v1,v2,v3,v4,a);
	double fb = func(v1,v2,v3,v4,b);
	double fc = func(v1,v2,v3,v4,c);	
	if (fabs(fc) < tol){//Check for convergence
		return c;
	}
	else {
		if (fa*fc<0){
			b = c;
		}
		else if (fb*fc<0){
			a = c;
		}
		else{
			;//todo: put some kind of error raising here?
		}
	}
	return bisection(func,v1,v2,v3,v4,a,b,tol);//Recurse
}

//Fitting
double sDerivative(QVector<double> xVec,QVector<double> yVec,QVector<double> ux,QVector<double> uy, double m){
	int n = xVec.size();
	//Initialise arrays
	double* w = new double[n];//"Overall" weights
	double* u = new double[n];
	double* v = new double[n];
	//Overall weights
	double sumW = 0;
	for (int i=0;i<n;i++){
		w[i] = (ux[i]*uy[i])/(pow(m,2)*uy[i]+ux[i]);
		sumW += w[i];
	}
	//Averages
	double sumWX = 0;
	double sumWY = 0;
	for (int i=0;i<n;i++){
		sumWX += w[i]*xVec[i];
		sumWY += w[i]*yVec[i];
	}
	double xA = sumWX/sumW;//Weighted average x
	double yA = sumWY/sumW;//Weighted average y
	//More derived quantities
	for (int i=0;i<n;i++){
		u[i] = xVec[i]-xA;
		v[i] = yVec[i]-yA;
	}
	//Final derived quantities
	double d = 0;
	for (int i=0;i<n;i++){
		d += pow(w[i],2)*pow(u[i],2)/ux[i];
	}
	double a = 0;	
	for (int i=0;i<n;i++){
		a += pow(w[i],2)*u[i]*v[i]/ux[i];
	}
	a = a*2/(3*d);
	double b = 0;	
	for (int i=0;i<n;i++){
		b += pow(w[i],2)*pow(v[i],2)/ux[i]-w[i]*pow(u[i],2);
	}
	b = b/(3*d);
	double g = 0;
	for (int i=0;i<n;i++){
		g += w[i]*u[i]*v[i];
	}
	g = -g/d;
	//Return
	return pow(m,3)-3*a*pow(m,2)+3*b*m-g;
}

//Without uncertainties
FitResult linearLS(QVector<double>xVec, QVector<double>yVec){
	int n = xVec.size();
	//Generate sums
	double sumX = 0;
	double sumY = 0;
	double sumX2 = 0;
	double sumXY = 0;
	for (int i=0;i<n;i++){
		sumX += xVec[i];
		sumY += yVec[i];
		sumX2 += pow(xVec[i],2);
		sumXY += xVec[i]*yVec[i];
	}
	//Calculate best fit parameters
	double denom = n*sumX2 - pow(sumX,2);
	double m = (n*sumXY-sumX*sumY)/denom;
	double c = (sumX2*sumY-sumX*sumXY)/denom;
	//Output fit
	//todo: include rms and such if this is used as main function for un-weighted fit
	FitResult fit;
	fit.m = m;
	fit.c = c;
	return fit;
}

//With uncertainties
FitResult linearLS(QVector<double> xVec,QVector<double> yVec,QVector<double> ux,QVector<double> uy){
	//Initialise fit struct
	FitResult fit;
	fit.fitSuccess=true;
	//
	int n = xVec.size();
	//Turn uncertainties into weights
	for(int i=0;i<n;i++){
		//The following switch from zero uncertainty to massive weight is essentially a hack, but is unbreakable with user input:
		if (ux[i] == 0){
			ux[i] = 1e50;
		}
		else {
			ux[i] = pow(ux[i],-2);
		}
		if (uy[i] == 0){
			uy[i] = 1e50;
		}
		else {
			uy[i] = pow(uy[i],-2);
		}		
	}
	//Hardcoded parameters
	double nGrid = 1000;
	double mWidth = 4;
	double tol = 0.00001;
	//Set up search interval information
	FitResult noU = linearLS(xVec,yVec);
	double m0 = noU.m;
	double uxMin = 0;
	double uxMax = 0;
	double uyMin = 0;
	double uyMax = 0;
	double usMax = 0;
	for (int i=0;i<n;i++){
		if(ux[i]>uxMax){
			uxMax = ux[i];
		}
		if(1/ux[i]>uxMin){
			uxMin = 1/ux[i];
		}
		if(uy[i]>uyMax){
			uyMax = uy[i];
		}
		if(1/uy[i]>uyMin){
			uyMin = 1/uy[i];
		}		
	}
	uxMin = 1/uxMin;
	uyMin = 1/uyMin;
	usMax = uxMax/uxMin > uyMax/uyMin ? uxMax/uxMin : uyMax/uyMin;
	QVector<double> ySort = yVec;
	sort(ySort.begin(),ySort.begin()+n);
	double yRange = ySort[n-1]-ySort[0];
	double dy = 0;
	for (int i=0;i<n-1;i++){
		if (ySort[i+1]-ySort[i]>dy){
			dy = ySort[i+1]-ySort[i];
		}
	}
	double dm = sqrt((1-(1/usMax)))*(1+m0)*dy/yRange;
	//Hardcoded, empirical choice of search interval
	double a = m0-4*dm;
	double b = m0+4*dm;
	//Grid search
	double dGrid = (b-a)/(nGrid-1);
	double fNew;
	double fOld = 0;
	QVector<int> iChanges;
	for (int i=0;i<nGrid;i++){
		fNew = sDerivative(xVec,yVec,ux,uy,a+i*dGrid);
		if (fNew*fOld < 0){
			iChanges.push_back(i);
		}
		fOld = fNew;
	}
	//Binary searches
	int nC = iChanges.size();
	QVector<double> mFinds;
	for (int i=0;i<nC;i++){
		double mL = a+(iChanges[i]-1)*dGrid;
		double mR = a+iChanges[i]*dGrid;
		mFinds.push_back(bisection(sDerivative,xVec,yVec,ux,uy,mL,mR,tol));	
	}
	//Assign best values
	double sumW;
	double xA;
	double yA;
	double sumWX;
	double sumWY;
	double c;
	double cBest;
	double osBest = 0;
	double s;
	double mBest = m0;
	for (int j=0;j<nC;j++){
		//Initialise arrays
		double* w = new double[n];
		//Overall weights
		sumW = 0;
		for (int i=0;i<n;i++){
			w[i] = (ux[i]*uy[i])/(pow(mFinds[j],2)*uy[i]+ux[i]);
			sumW += w[i];
		}
		//Averages
		sumWX = 0;
		sumWY = 0;
		for (int i=0;i<n;i++){
			sumWX += w[i]*xVec[i];
			sumWY += w[i]*yVec[i];
		}
		xA = sumWX/sumW;//Weighted average x
		yA = sumWY/sumW;//Weighted average y
		//
		c = yA-mFinds[j]*xA;
		s = 0;
		for (int i=0;i<n;i++){
			s += w[i]*(pow(mFinds[j]*xVec[i]+c-yVec[i],2));
		}
		s = s/sumW;
		//
		if (1/s > osBest){
			osBest = 1/s;
			mBest = mFinds[j];
			cBest = c;
		}
	}
	//Check if there's no change from case with no uncertainties
	if (mBest == m0){
		//
		cout << "no change" << endl;
		//Initialise arrays
		double* w = new double[n];
		//Overall weights
		sumW = 0;
		for (int i=0;i<n;i++){
			w[i] = (ux[i]*uy[i])/(pow(m0,2)*uy[i]+ux[i]);
			sumW += w[i];
		}
		//Averages
		sumWX = 0;
		sumWY = 0;
		for (int i=0;i<n;i++){
			sumWX += w[i]*xVec[i];
			sumWY += w[i]*yVec[i];
		}
		xA = sumWX/sumW;//Weighted average x
		yA = sumWY/sumW;//Weighted average y
		//
		cBest = yA-m0*xA;
		//
		//fit.fitSuccess = false;
	}
	
	//Add best parameters
	fit.m = mBest;
	fit.c = cBest;
	
	///////////////////////////////////////////////
	//start calculation of uncertainty in m and c//
	double* u = new double[n];
	double* v = new double[n];
	double* w = new double[n];
	sumW = 0;
	for (int i=0;i<n;i++){
		w[i] = (ux[i]*uy[i])/(pow(mBest,2)*uy[i]+ux[i]);
		sumW += w[i];
	}
	//Averages
	sumWX = 0;
	sumWY = 0;
	for (int i=0;i<n;i++){
		sumWX += w[i]*xVec[i];
		sumWY += w[i]*yVec[i];
	}
	xA = sumWX/sumW;//Weighted average x
	yA = sumWY/sumW;//Weighted average y
	//More derived quantities
	for (int i=0;i<n;i++){
		u[i] = xVec[i]-xA;
		v[i] = yVec[i]-yA;
	}
	//
	double umTerm1 = 0;
	double umTerm2 = 0;
	for (int i=0;i<n;i++){
		umTerm1 += w[i]*pow((mBest*u[i]-v[i]),2);
		umTerm2 += w[i]*pow(u[i],2);
	}
	double um2 = (1/(n-2.0))*umTerm1/umTerm2; //uncertainty in m squared
	//
	double ucTerm1 = 0;
	double ucTerm2 = 0;
	for (int i=0;i<n;i++){
		ucTerm1 += w[i]*pow(xVec[i],2);
		ucTerm2 += w[i];
	}
	double uc2 = (ucTerm1/ucTerm2)*um2;
	fit.um = sqrt(um2);
	fit.uc = sqrt(uc2);
	//end calculation of um and uc//
	////////////////////////////////

	//Fit metadata	
	double xBar=0, yBar=0, ssXX=0, ssXY=0, ssYY=0;
	double xMin=xVec[0], yMin=yVec[0], xMax=xVec[0], yMax=yVec[0];
	
	//Min and max calculation
	for (int i=0;i<n;i++)
	{
		xMin=std::min(xMin, xVec[i]);
		yMin=std::min(yMin, yVec[i]);
		xMax=std::max(xMax, xVec[i]);
		yMax=std::max(yMax, yVec[i]);
	}
	
	//R-squared calculation
	double RSS = 0;
	for (int i=0;i<n;i++){
		RSS += w[i]*pow(yVec[i]-cBest-mBest*xVec[i],2.0);
	}
	double TSS = 0;
	for (int i=0;i<n;i++){
		TSS += w[i]*pow(yVec[i]-yA,2.0);
	}
	
	//put metadata into fit object
	fit.rSquared = 1-RSS/TSS;
	fit.xBar=xA;
	fit.yBar=yA;
	fit.xMin=xMin;
	fit.yMin=yMin;
	fit.xMax=xMax;
	fit.yMax=yMax;

	//Return
	return fit;
}