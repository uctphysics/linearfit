#include <QtWidgets/QApplication>
#include <QPointer>
#include "fitcurve.h"

QPointer<FitCurve> w;

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	qDebug() << a.applicationName();
	FitCurve _w;	
	w = &_w;
	w->show();
	return a.exec();
}
