#include "fitcurve.h"
#include "fit.h"



FitCurve::FitCurve(QWidget *parent)
: QMainWindow(parent)
{
	ui.setupUi(this);

	//connect all the relevant slots and signals
	connect(ui.pbNew, SIGNAL( clicked() ), this, SLOT( newPushed() ) );
	connect(ui.pbImport, SIGNAL( clicked() ), this, SLOT( importPushed() ) );
	connect(ui.pbPrint, SIGNAL( clicked() ), this, SLOT( printPushed() ) );
	connect(ui.pbHelp, SIGNAL( clicked() ), this, SLOT( helpPushed() ) );
	connect(ui.pbExit, SIGNAL( clicked() ), this, SLOT( exitPushed() ) );
	connect(ui.tblData, SIGNAL( cellChanged(int, int)), this, SLOT( tableCellChanged(int, int) ) );
	connect(ui.chkXError, SIGNAL( stateChanged(int)), this, SLOT( changeXErrorState(int) ) );
	connect(ui.chkYError, SIGNAL( stateChanged(int)), this, SLOT( changeYErrorState(int) ) );

	//set the default table behaviour: no uncertainties, stretch out x and y columns
	enableXErrorColumn(false);
	enableYErrorColumn(false);
	ui.tblData->setColumnWidth(0, 250);
	ui.tblData->setColumnWidth(1, 250);
	//white background looks less depressing than a dark grey box
	ui.qwtPlot->setCanvasBackground(Qt::white);
    ((QFrame*)ui.qwtPlot->canvas())->setFrameStyle(QFrame::Box | QFrame::Plain );
    ((QFrame*)ui.qwtPlot->canvas())->setLineWidth(1);
	ui.qwtPlot->setAxisTitle(QwtPlot::xBottom, "x");
	ui.qwtPlot->setAxisTitle(QwtPlot::yLeft, "y");
	//set up the data points curve object

	dataPoints=new QwtCurveErrorXY("Data Points");
	dataPoints->setStyle(QwtPlotCurve::NoCurve);
	//symbol for data points, a black X works well. Using an X instead of a + so that it is clear that there are no errors specified
	//this symbol will only be plotted if there are no x/y errors, otherwise error bars are drawn (manually)
	QwtSymbol* symbol = new QwtSymbol();
	symbol->setSize(10, 10);
	symbol->setStyle(QwtSymbol::XCross);
	symbol->setPen(QColor(Qt::black));
    dataPoints->setSymbol(symbol);
	dataPoints->attach(ui.qwtPlot);

	//set up fit line object, dark blue, antialiased looks nice
	fitLine=new QwtPlotCurve("Line of best fit");
	fitLine->setPen(QColor(Qt::darkBlue));
	fitLine->setRenderHint(QwtPlotItem::RenderAntialiased);
	fitLine->attach(ui.qwtPlot);
	ui.qwtPlot->setAutoReplot(true);	


	//set up the font formats for the log
		//main title format
	titleFormat.setFontWeight(QFont::Bold);
	titleFormat.setForeground(QBrush(Qt::darkBlue));
	titleFormat.setFontPointSize(10);

	//heading format
	headingFormat.setFontWeight(QFont::Bold);
	headingFormat.setForeground(QBrush(Qt::black));
	headingFormat.setFontPointSize(8);
	headingFormat.setFontUnderline(true);
	
	//plain format
	plainFormat.setFontWeight(QFont::Normal);
	headingFormat.setForeground(QBrush(Qt::black));
	headingFormat.setFontPointSize(8);

	//error format, success format
	errorFormat=successFormat=plainFormat;
	errorFormat.setForeground(QBrush(Qt::red));
	successFormat.setForeground(QBrush(Qt::darkGreen));
}

void FitCurve::exitPushed()
{	
	//close

	//TODO ask before closing?
	if (QMessageBox::question(this, "Quit", "Are you sure you want to quit?", QMessageBox::Yes|QMessageBox::No)==QMessageBox::Yes)
		close();
}

void FitCurve::helpPushed()
{
	//open the help dialog
	QDialog* helpDialog=new QDialog(this);
	uiHelpDialog.setupUi(helpDialog);
	helpDialog->exec();
	delete helpDialog;

}

void FitCurve::newPushed()
{
	//confirm message box
	if (QMessageBox::question(this, "New Fit", "Are you sure you want to start a new fit?\nAll current data will be erased.", QMessageBox::Yes|QMessageBox::No)==QMessageBox::Yes)
	{
		while (ui.tblData->rowCount()>0)
			ui.tblData->removeRow(0);
		//add the two default rows
		ui.tblData->insertRow(0);
		ui.tblData->insertRow(0);
		//force an update
		tableCellChanged(0,0);

		//clear log
		ui.txtLog->clear();
	}
}


//imports from a CSV (for example, from excel)
void FitCurve::importPushed()
{
	//create an open file dialog
	QString fileName = QFileDialog::getOpenFileName(this, tr("Import"),
                                                 QDir::currentPath(),
                                                  tr("CSV Files (*.csv);;Text files (*.txt);;All files (*.*)"));


	//QFileDialog* openDialog=new QFileDialog(this, "Import", QDir::currentPath(), tr("CSV Files (*.csv);;Text files (*.txt);;All files (*.*)"));
	//if OK was pressed
	if (fileName!=0 && !fileName.isEmpty())
	{
			QFile file(fileName);
			 if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
			 {
				 QMessageBox::critical(this, "Error", "Could not read file!", QMessageBox::Ok);
				 return;
			 }
			QVector<QStringList> entries;
			//variable to keep track of the numner of columns. If this is inconsistent or greater than 4, loading will be aborted
			int numColumns=-1;
			while (!file.atEnd()) 
			{
				QString line = file.readLine();
				QStringList tempList=line.split(",");
				//numColumns hasn't been assigned yet, set it to the current number of columns
				if (numColumns==-1)
					numColumns=tempList.count();
				//if there's a problem with the columns
				if (numColumns!=tempList.count() || numColumns>4)
				{
					QMessageBox::critical(this, "Error", "The number of columns in the CSV is inconsistent!", QMessageBox::Ok);
					file.close();
					return;
				}

				//if everything's ok, add it to the entries vector
				entries.push_back(tempList);					
			}
			file.close();
			//now, presumably the entries are all sensible. Now we clear the table and populate it from the entries

			//just a brief sanity check to make sure we've got the right number of columns
			if (numColumns>=2 && numColumns<=4)
			{
				//clear table
				while (ui.tblData->rowCount()>0)
					ui.tblData->removeRow(0);

				//for each line
				for (int i=0;i<entries.count();i++)
				{
					ui.tblData->insertRow(i);
					for (int j=0;j<numColumns;j++)
					{
						//create an item for the cell
						QTableWidgetItem* tempItem=new QTableWidgetItem((entries[i])[j]);
						ui.tblData->setItem(i, j, tempItem);
					}
				}
				//fire off changed event
				tableCellChanged(0,0);
			}
			else
			{
				QMessageBox::critical(this, "Error", "The number of columns in the CSV is incorrect!", QMessageBox::Ok);
				return;
			}		
	}
}

void FitCurve::printPushed()
{
	
	//select what to print
    PrintDialog* printDialog=new PrintDialog(this);
	if (printDialog->exec()!=QDialog::Accepted)
		return;

	 QPrinter printer;
	
	 //select printer
     QPrintDialog *dialog = new QPrintDialog(&printer, 0);
     dialog->setWindowTitle(tr("Print Document"));    
     if (dialog->exec() != QDialog::Accepted)
         return;

	 if (printDialog->chkCombined->isChecked())
	 {
		 printer.setOrientation(QPrinter::Landscape);
		 auto p = this->ui.centralWidget->palette();
		 auto pWhite = p;
		 pWhite.setBrush(QPalette::Background, QColor(Qt::white));
		 this->ui.centralWidget->setPalette(pWhite);
		 //hide buttons etc
		 this->ui.chkXError->setVisible(false);
		 this->ui.chkYError->setVisible(false);

		 this->ui.pbNew->setVisible(false);
		 this->ui.pbImport->setVisible(false);
		 this->ui.pbPrint->setVisible(false);
		 this->ui.pbHelp->setVisible(false);
		 this->ui.pbExit->setVisible(false);

		 QPixmap pixmap = this->grab();

		 this->ui.centralWidget->setPalette(p);
		 //show buttons again
		 this->ui.chkXError->setVisible(true);
		 this->ui.chkYError->setVisible(true);

		 this->ui.pbNew->setVisible(true);
		 this->ui.pbImport->setVisible(true);
		 this->ui.pbPrint->setVisible(true);
		 this->ui.pbHelp->setVisible(true);
		 this->ui.pbExit->setVisible(true);


		 QPainter painter;
		 painter.begin(&printer);
		 painter.drawPixmap(0, 0, pixmap);
		 painter.drawText(10, pixmap.height(), printDialog->lineEditStudentNumber->text());
		 painter.end();

		 


		 dialog->close();
		 delete dialog;
		 return;
	 }
	 //print graph (if selected)
	 if (printDialog->chkGraph->isChecked())
	 {
		printer.setOrientation(QPrinter::Landscape);
        QwtPlotRenderer renderer;

        if ( printer.colorMode() == QPrinter::GrayScale )
        {
            renderer.setDiscardFlag( QwtPlotRenderer::DiscardBackground );
            renderer.setDiscardFlag( QwtPlotRenderer::DiscardCanvasBackground );
            renderer.setDiscardFlag( QwtPlotRenderer::DiscardCanvasFrame );
            renderer.setLayoutFlag( QwtPlotRenderer::FrameWithScales );
        }

        renderer.renderTo( ui.qwtPlot, printer );
	 }

	 //print table (if selected)
	 if (printDialog->chkTable->isChecked())
	 {
		 printer.setOrientation(QPrinter::Portrait);
		
		 //create a rich text document
		 QTextDocument* docTable=new QTextDocument();
		QTextCursor cursorTable(docTable);
		
		
		//simple border
		QTextTableFormat tableFormat;
		tableFormat.setAlignment(Qt::AlignVCenter);
		tableFormat.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
		//work out how many rows and columns are needed. We don't need to print out empty rows
		int numRows=0;
		for (int i=0;i<ui.tblData->rowCount();i++)
			if (!isEmptyRow(i))
				numRows++;

		//need x & y columns, as well as uX and uY if needed
		int numColumns=2+(useXErrors?1:0)+(useYErrors?1:0);
		
		//create the table. Add one row for the headings
		QTextTable *table=cursorTable.insertTable(numRows+1, numColumns, tableFormat);
		
		//border format
		QTextFrameFormat frameFormat = cursorTable.currentFrame()->frameFormat();
		frameFormat.setBorder(1);
		cursorTable.currentFrame()->setFrameFormat(frameFormat);
	
		//insert heading
		table->cellAt(0, 0).firstCursorPosition().insertText("\tX\t");
		table->cellAt(0, 1).firstCursorPosition().insertText("\tY\t");
		if (useXErrors)
			table->cellAt(0, 2).firstCursorPosition().insertText("\tuX\t");
		if (useYErrors)
			table->cellAt(0, (useXErrors?3:2)).firstCursorPosition().insertText("\tuY\t");

		//insert values. need a separate counter for the destination row as we're skipping empty rows
		for (int i=0, destRow=1;i<ui.tblData->rowCount();i++)
		{

			if (!isEmptyRow(i))
			{
				
				//add x (0) and y (1) values
				for (int j=0;j<2;j++)
				{
					QTableWidgetItem* tempItem=ui.tblData->item(i, j);
					if (tempItem!=0)
						table->cellAt(destRow, j).firstCursorPosition().insertText(tempItem->text());
				}
				//insert dx / dy values if necessary
				if (useXErrors)
				{
					QTableWidgetItem* tempItem=ui.tblData->item(i, 2);
					if (tempItem!=0)
						table->cellAt(destRow, 2).firstCursorPosition().insertText(tempItem->text());
				}
				if (useYErrors)
				{
					QTableWidgetItem* tempItem=ui.tblData->item(i, 3);
					if (tempItem!=0)
					//the destination column depends on whether we're using x errors as well!
						table->cellAt(destRow, (useXErrors?3:2)).firstCursorPosition().insertText(tempItem->text());
				}
				//only go to the next row if we've actually added to the table
				destRow++;
			}
		}
		
		docTable->print(&printer);
	 }

	 //print results 
	 if (printDialog->chkResults->isChecked())
	 {
		 printer.setOrientation(QPrinter::Portrait);
		 ui.txtLog->print(&printer);
	 }

	 dialog->close();
	 delete dialog;
}

void FitCurve::updateFitLog()
{
	//clear old log
	ui.txtLog->clear();
	//some rich text to add colours to results

	//set the heading: depends on check settings
	//neither
	if (!ui.chkXError->isChecked() && !ui.chkYError->isChecked())
		ui.txtLog->textCursor().insertText("Linear Fit without errors:\n\n", titleFormat);
	//just one of the two
	else if (ui.chkXError->isChecked() ^ ui.chkYError->isChecked())
		ui.txtLog->textCursor().insertText(QString("Linear Fit using ")+(ui.chkXError->isChecked()?"X":"Y")+" errors:\n\n", titleFormat);
	else
		ui.txtLog->textCursor().insertText("Linear Fit using X and Y errors:\n\n", titleFormat);

	//display results (if fit was successful & have enough points)
	if (updatedFit.fitSuccess && xVector.size()>1)
	{
		//results
		ui.txtLog->textCursor().insertText("Best fit values:\n", headingFormat);
		if (updatedFit.um==0 || updatedFit.um!= updatedFit.um)
			ui.txtLog->textCursor().insertText("m=" + QString::number(updatedFit.m) + "\n", plainFormat);
		else
			ui.txtLog->textCursor().insertText("m="+QString::number(updatedFit.m)+QChar(0x00B1) + QString::number(updatedFit.um)+"\n", plainFormat);
		if (updatedFit.uc == 0 || updatedFit.uc != updatedFit.uc)
			ui.txtLog->textCursor().insertText("c="+QString::number(updatedFit.c)+"\n\n", plainFormat);
		else
			ui.txtLog->textCursor().insertText("c=" + QString::number(updatedFit.c) + QChar(0x00B1) + QString::number(updatedFit.uc) + "\n\n", plainFormat);

		//r squared
		ui.txtLog->textCursor().insertText("Correlation:\n", headingFormat);
		ui.txtLog->textCursor().insertText("R"+QString(QChar(0x00B2)) + " = " + QString::number(updatedFit.rSquared)+((updatedFit.rSquared>0.6)?" [good fit]":" [bad fit]"), (updatedFit.rSquared>0.6)?successFormat:errorFormat);
	}
	//fit was unsuccessful but there were enough points
	else if (!updatedFit.fitSuccess && xVector.size()>1)	
		ui.txtLog->textCursor().insertText("There was a problem fitting data. Please check your table entries.", errorFormat);		
	else
		ui.txtLog->textCursor().insertText("Not enough data. Fit requires at least two points. Make sure you have completed all the necessary entries.\nIf fitting with errors, each point must have a specified error.", errorFormat);

}

void FitCurve::changeXErrorState(int state)
{
	//if unchecked, hide x column
	if (state==Qt::Unchecked)
	{

		enableXErrorColumn(false);

		//work out the width of the columns based on the number of visible columns.
		int width=(ui.tblData->width()-BORDER)/((ui.chkYError->isChecked())?3:2);
		//resize columns
		ui.tblData->setColumnWidth(0, width);
		ui.tblData->setColumnWidth(1, width);
		if (ui.chkYError->isChecked())
			ui.tblData->setColumnWidth(3, width);

	}
	if (state==Qt::Checked)
	{
		enableXErrorColumn(true);

		//work out the width of the columns based on the number of visible columns. If we're showing dX again we must show dY again IFF y checkbox is checked
		int width=(ui.tblData->width()-BORDER)/((ui.chkYError->isChecked())?4:3);
		//resize columns
		ui.tblData->setColumnWidth(0, width);
		ui.tblData->setColumnWidth(1, width);
		ui.tblData->setColumnWidth(2, width);
		if (ui.chkYError->isChecked())
			ui.tblData->setColumnWidth(3, width);
	}
	//fire a cell changed event, to force a replot
	tableCellChanged(0,0);

}

void FitCurve::changeYErrorState(int state)
{
	if (state==Qt::Unchecked)
	{
		enableYErrorColumn(false);

		//work out the width of the columns based on the number of visible columns. If dY is not shown, it's either 2 or 3, depending on whether dX should be shown
		int width=(ui.tblData->width()-BORDER)/((ui.chkXError->isChecked())?3:2);
		//resize columns
		ui.tblData->setColumnWidth(0, width);
		ui.tblData->setColumnWidth(1, width);
		if (ui.chkXError->isChecked())
			ui.tblData->setColumnWidth(2, width);
	}
	if (state==Qt::Checked)
	{
		enableYErrorColumn(true);

		//work out the width of the columns based on the number of visible columns. If dY is shown, dX must also be shown
		int width=(ui.tblData->width()-BORDER)/((ui.chkXError->isChecked())?4:3);
		//resize columns
		ui.tblData->setColumnWidth(0, width);
		ui.tblData->setColumnWidth(1, width);
		if (ui.chkXError->isChecked())
			ui.tblData->setColumnWidth(2, width);
		ui.tblData->setColumnWidth(3, width);
	}
	//fire a cell changed event, to force a replot
	tableCellChanged(0,0);
}

void FitCurve::enableXErrorColumn(bool enable)
{
	useXErrors=enable;
	//show/hide the dX columns
	if (enable)
		ui.tblData->showColumn(2);
	else
		ui.tblData->hideColumn(2);
}

void FitCurve::enableYErrorColumn(bool enable)
{
	useYErrors=enable;
	//show/hide the dY columns
	if (enable)
		ui.tblData->showColumn(3);
	else
		ui.tblData->hideColumn(3);
}

void FitCurve::tableCellChanged(int row, int column)
{
	//static variable, used to check if a cell change is already being processed (this prevents nested cellChanged() events being processed)
	static bool busyChanging=false;

	//if we aren't busy with a change already
	if (!busyChanging)
	{
		//now we're busy
		busyChanging=true;
		//update based on new data
		int numRows=ui.tblData->rowCount();

		//clear vectors to be sent to the fitting routine
		xVector.clear();
		yVector.clear();
		uxVector.clear();
		uyVector.clear();
		//fill vectors up with valid data
		for (int i=0;i<numRows;i++)
		{
			double xVal, yVal, dxVal, dyVal;
			//basically checks if each relevant cell is valid. Thus, it first checks if we're using x/y errors before checking whether those cells are valid.
			//If they're invalid and we're not using errors, we don't care
			bool validRow=(isValidCell(i, 0, xVal)) && (isValidCell(i, 1, yVal)) && (!useXErrors || (isValidCell(i, 2, dxVal))) && (!useYErrors || (isValidCell(i, 3, dyVal)));

			//add data to vectors if it's a valid row
			if (validRow)
			{
				xVector.push_back(xVal);
				yVector.push_back(yVal);
				//add errors, if we're using them (take abs value)
				if (useXErrors)
					uxVector.push_back(abs(dxVal));
				if (useYErrors)
					uyVector.push_back(abs(dyVal));

			}
			//if row is invalid, highlight it. If row is valid, undo all highlighting
			for (int j=0;j<4;j++)
			{
				//get the item at specified index
				QTableWidgetItem* tempItem=ui.tblData->item(i, j);
				//if there's no item there, just create an empty item (unless it's an empty row)
				if (tempItem==0 && !isEmptyRow(i))
				{
					tempItem=new QTableWidgetItem();
					ui.tblData->setItem(i, j, tempItem);
				}
				//if there's now an item there
				if (tempItem!=0)
					//no point highlighting empty rows, so first check if the row is both valid and non-empty
					if (!validRow && !isEmptyRow(i))
						tempItem->setBackgroundColor(QColor(255, 150, 150));
					else
						tempItem->setBackgroundColor(Qt::transparent);
			}
		}	

		//plot the points with error bars. If empty vectors are passed in for ux and uy, the QwtCurveErrorXY object just does a normal X
		dataPoints->setData(xVector, yVector, uxVector, uyVector);		
		//only update if there are 3 or more points
		if (xVector.size()>=2)
		{
			//update the fit. If we're not using a column, just pass in a vector of zeros
			if (useXErrors && useYErrors)
				updatedFit=linearLS(xVector, yVector, uxVector, uyVector);	
			else if (useXErrors)
				updatedFit=linearLS(xVector, yVector, uxVector, QVector<double>(xVector.size(),0));	
			else if (useYErrors)
				updatedFit=linearLS(xVector, yVector, QVector<double>(yVector.size(),0), uyVector);	
			else
				updatedFit=linearLS(xVector, yVector, QVector<double>(yVector.size(),0), QVector<double>(xVector.size(),0));	
		}
		//update the log with the latest fit
		updateFitLog();

		if (updatedFit.fitSuccess)
		{
			//fit points (extend the range to 5% above max / 5% below min)
			double range=updatedFit.xMax-updatedFit.xMin;
			double xFitVals[]={updatedFit.xMin-0.05*range, updatedFit.xMax+0.05*range};
			double yFitVals[]={updatedFit.m*xFitVals[0]+updatedFit.c, updatedFit.m*xFitVals[1]+updatedFit.c};
            fitLine->setSamples(xFitVals, yFitVals, 2);
		}
		else
		{
			//clear fit line data if fit was unsuccessful
            fitLine->setSamples(0, 0, 0);
		}

		//Insert a row below if user enters data in the last row. This way, you'll always have enough rows and never need to manually add them. One doesn't expect the user to remove lots of rows,
		// so rows containing empty or invalid entries are not counted. First we have to make sure that the newly added item isn't empty
		if (row==numRows-1 &&!ui.tblData->item(row, column)->text().isEmpty())
			ui.tblData->insertRow(row+1);
		
		//finished changing
		busyChanging=false;
	}

}

bool FitCurve::isValidCell(int row, int column, double& value)
{
	//get the item at specified index
	QTableWidgetItem* tempItem=ui.tblData->item(row, column);
	//if it's empty
	if (tempItem==0 || tempItem->text().isEmpty())
		return false;
	else
	{
		//check if it's a valid double
		bool isValid;
		value=tempItem->text().toDouble(&isValid);
		
		//can't have zero entry for uncertainty (column 2 (x) or column 3 (y))
		if (isValid && column==2 && useXErrors && value<=0)
			return false;
		else if (isValid && column==3 && useYErrors && value<=0)
			return false;
		
		return isValid;
	}
}

//checks to see if a given row is empty
bool FitCurve::isEmptyRow(int row)
{
	for (int i=0;i<4;i++)
	{
		QTableWidgetItem* tempItem=ui.tblData->item(row, i);
		//if it's not empty, return false
		if (tempItem!=0 && !tempItem->text().isEmpty())
			return false;
	}
	//otherwise return true
	return true;
}

//a little housekeeping
FitCurve::~FitCurve()
{
	delete dataPoints;
	delete fitLine;
}
