#ifndef QTABLEEDITOR_H
#define QTABLEEDITOR_H

#include <QTableWidget>
#include <QKeyEvent>

class QTableEditor : public QTableWidget
{
	Q_OBJECT

public:
	QTableEditor(QWidget *parent);
	~QTableEditor();
 protected:
    void keyPressEvent(QKeyEvent * event);
private:
	
};

#endif // QTABLEEDITOR_H
