#include "qwt_curve_error_xy.h"

#define CAP_SIZE 5

//by default, no data=>no error bars
QwtCurveErrorXY::QwtCurveErrorXY(const QString &title):
    QwtPlotCurve(QwtText(title))
{
	showXError=false;
	showYError=false;

}


void QwtCurveErrorXY::setData(const QVector<double> &xData, 
    const QVector<double> &yData, const QVector<double> &uxData, const QVector<double> &uyData)
{
	//update data. If there are errors then they must be shown
	showXError= (!uxData.isEmpty());
	showYError= (!uyData.isEmpty());
	d_x=xData;
	d_y=yData;
	d_ux=uxData;
	d_uy=uyData;
	//set the x & y data via the superclass for plotting purposes
    QwtPlotCurve::setSamples(xData, yData);
}

//overloading draw() to include manually drawn x AND y error bars. QWT 6 supports display of y-errors but not x-errors. That's no good
void QwtCurveErrorXY::draw(QPainter *painter,
    const QwtScaleMap &xMap, const QwtScaleMap &yMap,
    const QRectF &canvasRect) const
{
	//if there's nothing to paint, leave
    if ( !painter || dataSize() <= 0 )
        return;

	//draw each point
	int N=d_x.size();
	for (int i=0;i<N;i++)
	{
		if (showXError)
		{
			//draw x bar
			painter->drawLine(xMap.transform(d_x[i]-d_ux[i]), yMap.transform(d_y[i]), xMap.transform(d_x[i]+d_ux[i]), yMap.transform(d_y[i]));
			//draw caps:
			//left
			painter->drawLine(xMap.transform(d_x[i]-d_ux[i]), yMap.transform(d_y[i])-CAP_SIZE/2, xMap.transform(d_x[i]-d_ux[i]), yMap.transform(d_y[i])+CAP_SIZE/2);
			//right
			painter->drawLine(xMap.transform(d_x[i]+d_ux[i]), yMap.transform(d_y[i])-CAP_SIZE/2, xMap.transform(d_x[i]+d_ux[i]), yMap.transform(d_y[i])+CAP_SIZE/2);
		}
		if (showYError)
		{
			//draw y bar
			painter->drawLine(xMap.transform(d_x[i]), yMap.transform(d_y[i]-d_uy[i]), xMap.transform(d_x[i]), yMap.transform(d_y[i]+d_uy[i]));
			//draw caps:
			//bottom
			painter->drawLine(xMap.transform(d_x[i])-CAP_SIZE/2, yMap.transform(d_y[i]-d_uy[i]), xMap.transform(d_x[i])+CAP_SIZE/2, yMap.transform(d_y[i]-d_uy[i]));
			//top
			painter->drawLine(xMap.transform(d_x[i])-CAP_SIZE/2, yMap.transform(d_y[i]+d_uy[i]), xMap.transform(d_x[i])+CAP_SIZE/2, yMap.transform(d_y[i]+d_uy[i]));

		}
	}
	//only draw standard cross if there are no error bars
	if (!showXError && !showYError)
        QwtPlotCurve::draw(painter, xMap, yMap, canvasRect);


}

//overload how the bounding box is calculated
QwtDoubleRect QwtCurveErrorXY::boundingRect() const
{
	//some error values (and 2 or more points)
    if ((showXError || showYError) && d_x.size()>1)
	{

		//if we have error bars, the bounding box needs to be expanded to encompass those error bars.
		//for this, we need to find the max/min vals, as well as the errors of those values.
		//initialise needed variables
		double xMin=d_x[0], yMin=d_y[0], xMax=d_x[0], yMax=d_y[0];
		//run through each data element
		int N=d_x.size();
		for (int i=0;i<N;i++)
		{
			//if there are X errors, expand max/min range
			if (showXError)
			{
				xMin=std::min(d_x[i]-d_ux[i], xMin);
				xMax=std::max(d_x[i]+d_ux[i], xMax);
			}
			else
			{
				xMin=std::min(d_x[i], xMin);
				xMax=std::max(d_x[i], xMax);
			}

			if (showYError)
			{
				yMin=std::min(d_y[i]-d_uy[i], yMin);
				yMax=std::max(d_y[i]+d_uy[i], yMax);
			}
			else
			{
				yMin=std::min(d_y[i], yMin);
				yMax=std::max(d_y[i], yMax);
			}
		
		}
		//x,y,width,length
		return QwtDoubleRect(xMin, yMin, xMax-xMin, yMax-yMin);
	}
	else
		return QwtPlotCurve::boundingRect();
}


QwtCurveErrorXY::~QwtCurveErrorXY()
{

}
