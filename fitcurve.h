#ifndef FITCURVE_H
#define FITCURVE_H

#define BORDER 20

#include <math.h>
#include <QMainWindow>
#include <qwt_plot_curve.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_curve.h>
#include <qwt_text.h>
#include <qwt_math.h>
#include <qwt_symbol.h>
#include <qwt_plot_renderer.h>
#include "ui_fitcurve.h"
#include "ui_HelpDialog.h"
#include "qwt_curve_error_xy.h"
#include "printdialog.h"
#include <QVector>
#include <QString>
#include <QTextCursor>
 #include <QTextCharFormat>
 #include <QFileDialog>
#include <QPrinter>
#include <QPrintDialog>
#include <QFile>
#include <QMessageBox>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextTable>

struct FitResult
{
	double xBar, yBar;
	double xMin, yMin;
	double xMax, yMax;
	double m, c, um, uc;
	double rSquared;
	bool fitSuccess;
};

class FitCurve : public QMainWindow
{
	Q_OBJECT

public:
    FitCurve(QWidget *parent = 0);
	void enableXErrorColumn(bool enable);
	void enableYErrorColumn(bool enable);
	~FitCurve();
	public slots:;
	void newPushed();
	void exitPushed();
	void printPushed();
	void importPushed();
	void helpPushed();
	void tableCellChanged(int row, int column);
	void changeXErrorState(int state);
	void changeYErrorState(int state);

private:
	bool useXErrors, useYErrors;
	bool isValidCell(int row, int column, double& value);
	bool isEmptyRow(int row);
	void updateFitLog();
	
	Ui::FitCurveClass ui;
	Ui::Dialog uiHelpDialog;

	QVector<double>xVector;
	QVector<double>yVector;
	QVector<double>uxVector;
	QVector<double>uyVector;
	QwtCurveErrorXY* dataPoints;
	QwtPlotCurve* fitLine;
	FitResult updatedFit;

	//text formats for log
	QTextCharFormat titleFormat, headingFormat, errorFormat, plainFormat, successFormat;
};

#endif // FITCURVE_H
