#ifndef QWTCURVEERRORXY_H
#define QWTCURVEERRORXY_H

#include <qwt_plot_curve.h>
#include <qwt_scale_map.h>
#include <qwt_compat.h>
#include <QVector>
#include <QLine>
#include <QPainter>

class QwtCurveErrorXY : public QwtPlotCurve
{

public:
	explicit QwtCurveErrorXY();
    explicit QwtCurveErrorXY(const QwtText &title);
    explicit QwtCurveErrorXY(const QString &title);
    void setData(const QVector<double> &xData, const QVector<double> &yData, const QVector<double> &uxData, const QVector<double> &uyData);
	QwtDoubleRect boundingRect() const;
	~QwtCurveErrorXY();

protected:
    void draw(QPainter *painter, const QwtScaleMap &xMap, const QwtScaleMap &yMap, 		const QRectF & 	canvasRect) const;

private:
	QVector<double> d_x;
	QVector<double> d_y;
	QVector<double> d_ux;
    QVector<double> d_uy;
	bool showXError;
	bool showYError;

};

#endif // QWTCURVEERRORXY_H
