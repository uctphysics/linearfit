# README #

### Features: ###

LeetFit is a straight-line fitting tool for use in teaching laboratories. It allows users to specify a data set of x and y values to fit, as well as uncertainties in either x, y or both x and y. A graphical plot of the data points as well as the fitted line is shown as input is entered. The calculated fit parameters are updated whenever new data is entered. Uncertainties in the gradient and intercept, as well as the correlation coefficient R², are also shown.
Data can be imported directly from a comma separated value (CSV) file or entered manually. The table of data, fit information and plot can all be printed. 

### Getting Started: ###


To enter data manually, type the entries in the table on the right. The graph will automatically be plotted once two or more points have been entered. 
To import data from a CSV, simply select the "Import" button and select the file to import data from. All data must be in 2, 3 or 4 column format. Any data found that is not valid (such as headings or comments) will be shown but will not be used in the fitting process. 
Clicking on the weighting checkboxes will toggle the use of uncertainty-weighting in the fit and will toggle the display of the Δx and Δy columns.


### Data Restrictions: ###

Valid data must be entered for points to be included in the fit. Entries consisting of words or incorrectly formatted numbers will not be included. If a weighting in x or y has been enabled, all data points must contain the relevant uncertainties to be included in the fit. Uncertainties must be non-zero. Three or more points are required for a fit.

### Building: ###

LinearFit requires Qt>5.8 and Qwt > 6.1.3. Make sure to add the Qwt include and lib directories to the relevant paths.