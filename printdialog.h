#ifndef PRINTDIALOG_H
#define PRINTDIALOG_H

#include <QDialog>
#include <QKeyEvent>
#include <QPushButton>
#include <QCheckBox>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QCheckBox>
#include <QLineEdit>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QVBoxLayout>

class PrintDialog : public QDialog 
{
	Q_OBJECT
	QVBoxLayout *vLayoutForm;
    QVBoxLayout *vLayout;
    QHBoxLayout *hLayout;
    QPushButton *btnOK, *btnCancel;
public:
	QCheckBox *chkCombined;
	QCheckBox *chkGraph;
    QCheckBox *chkTable;
    QCheckBox *chkResults;
	QLineEdit *lineEditStudentNumber;
	
public slots:
	void updateButtons();
    
public:
	PrintDialog( QWidget *parent);


};


#endif // PRINTDIALOG_H
