#include "qtableeditor.h"

QTableEditor::QTableEditor(QWidget *parent)
	: QTableWidget(parent)
{

}
//only real difference between a "QTableEditor" and a QTableWidget: if you press delete it deletes the current item, if you press enter it edits.
void QTableEditor::keyPressEvent(QKeyEvent * event)
{
	QTableWidgetItem* currentSelectedItem=currentItem();
	if (event->key()==Qt::Key_Delete)
	{
		//only delete if item exits
		if (currentSelectedItem!=0)
		{
			currentSelectedItem->setText("");
		}
	}
	else if (event->key()==Qt::Key_Enter)
	{
		//only edit if item exists
		if (currentSelectedItem!=0)
			this->editItem(currentSelectedItem);
	}
	else
		QTableWidget::keyPressEvent(event);
}


QTableEditor::~QTableEditor()
{

}
