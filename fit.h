/// \file fit.h
/// \author Michael Malahe
/// \brief Fitting functions.

#ifndef _FIT_H
#define	_FIT_H

#include <algorithm>
#include <QVector>
#include <cmath>
#include <iostream>
#include "fitcurve.h"
using namespace std;

//Utility
double testFunc(double x);
double bisection(double (*func)(double),double a,double b,double tol);
double bisection(double (*func)(QVector<double>,QVector<double>,QVector<double>,QVector<double>,double),QVector<double> v1,QVector<double> v2,QVector<double> v3,QVector<double> v4,double a,double b,double tol);

//Fitting
FitResult linearLS(QVector<double> xVec, QVector<double>yVec);
FitResult linearLS(QVector<double> xVec,QVector<double> yVec,QVector<double> ux,QVector<double> uy);
double sDerivative(QVector<double> xVec,QVector<double> yVec,QVector<double> ux,QVector<double> uy, double m);

#endif	/* _ALGUTIL_H */