#include "printdialog.h"

   PrintDialog::PrintDialog( QWidget *parent)
        : QDialog( parent)
    {

		//set a fixed size
        //this->resize(250, 114);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        this->setSizePolicy(sizePolicy);

		//layouts
        vLayoutForm = new QVBoxLayout(this);
        vLayout = new QVBoxLayout();
        hLayout = new QHBoxLayout();


		//check boxes
		chkCombined = new QCheckBox("Combined print", this);
		chkCombined->setChecked(true);
		chkGraph = new QCheckBox("Print Graph", this);
        chkTable = new QCheckBox("Print Table", this);
        chkResults = new QCheckBox("Print Results", this);
		chkGraph->setEnabled(false);
		chkTable->setEnabled(false);
		chkResults->setEnabled(false);

		lineEditStudentNumber = new QLineEdit(this);
		lineEditStudentNumber->setPlaceholderText("Enter student number");
		lineEditStudentNumber->setFocus();
		//buttons
        btnOK=new QPushButton("OK", this);
		btnCancel=new QPushButton("Cancel", this);
        
		//add widgets to correct layouts
		vLayout->addWidget(chkCombined);
		vLayout->addWidget(chkGraph);
        vLayout->addWidget(chkTable);
        vLayout->addWidget(chkResults);
		vLayout->addWidget(lineEditStudentNumber);

		hLayout->addWidget(btnOK);
		hLayout->addWidget(btnCancel);

		//add layouts to parent layouts
		vLayout->addLayout(hLayout);
        vLayoutForm->addLayout(vLayout);
		
		//connect slots
        QObject::connect(btnOK, SIGNAL(pressed()), this, SLOT(accept()));
        QObject::connect(btnCancel, SIGNAL(pressed()), this, SLOT(reject()));

		QObject::connect(chkCombined, SIGNAL(stateChanged(int)), this, SLOT(updateButtons()));
		QObject::connect(chkGraph, SIGNAL(stateChanged(int)),this, SLOT (updateButtons()));
		QObject::connect(chkTable, SIGNAL(stateChanged(int)), this, SLOT (updateButtons()));
		QObject::connect(chkResults, SIGNAL(stateChanged(int)), this, SLOT (updateButtons()));
    }


   void PrintDialog::updateButtons()
   {
	   if (chkCombined->isChecked() || chkGraph->isChecked() ||chkTable->isChecked() || chkResults->isChecked())
		   btnOK->setEnabled(true);
	   else
		   btnOK->setEnabled(false);
	   chkGraph->setEnabled(!chkCombined->isChecked());
	   chkTable->setEnabled(!chkCombined->isChecked());
	   chkResults->setEnabled(!chkCombined->isChecked());
   }
